/*
 * Copyright 2021 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.ruckus;

import com.iamcodepoet.ruckus.model.Buffering;
import com.iamcodepoet.ruckus.model.Device;
import com.iamcodepoet.ruckus.model.Format;
import com.iamcodepoet.ruckus.model.NewStream;
import com.iamcodepoet.ruckus.model.Player;
import com.iamcodepoet.ruckus.model.Plugin;
import com.iamcodepoet.ruckus.model.StreamSegment;
import org.junit.jupiter.api.Assertions;

/**
 * A helper class to run unit tests.
 *
 * @author Roberto C. Benitez
 */
public final class TestHelper
{

    public static void testPlayer(Player player)
    {
        Assertions.assertNotNull(player, "Player element is null");

        Assertions.assertEquals(false, player.isError(), "Invalid player property 'error'");
        Assertions.assertEquals("play", player.getState(), "Invlid player property 'state");

        Plugin plugin = player.getPlugin();
        Assertions.assertNotNull(plugin, "plugin element is null");

        Format format = player.getFormat();
        Assertions.assertNotNull(format, "format element is null");

        Buffering buffering = player.getBuffering();
        Assertions.assertNotNull(buffering, "buffering element is null");

        NewStream newStream = player.getNewStream();
        Assertions.assertNotNull(newStream, "new_stream property is null");
        Assertions.assertEquals("252906 ms", player.getPosition(), "Invalid position");
        Assertions.assertEquals("1213679 ms", player.getDuration(), "Invalid duration");
        Assertions.assertEquals(false, player.isLive(), "Invalid _is_live property");

        StreamSegment streamSegment = player.getStreamSegment();
        Assertions.assertNotNull(streamSegment, "stream_segment element is null");
    }

    public static void testDevice(Device device)
    {
        Assertions.assertNotNull(device, "Roku Device is null");
        Assertions.assertEquals("Sauna Roku", device.getUserDeviceName(), "Invalid user device name");
        Assertions.assertEquals("Roku Ultra", device.getModelName(), "Invalid model name");
    }

    private TestHelper()
    {
    }
}
