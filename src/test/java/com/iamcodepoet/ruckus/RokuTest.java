/*
 * Copyright 2021 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.ruckus;

import com.iamcodepoet.ruckus.model.Device;
import com.iamcodepoet.ruckus.model.Player;
import com.iamcodepoet.ruckus.model.RokuApp;
import com.iamcodepoet.ruckus.parse.ParserTest;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author Roberto C. Benitez
 */
public class RokuTest extends BaseTest
{

    @Mock
    private Roku mockRoku;

    public RokuTest()
    {
    }

    @BeforeEach
    public void setupMockups()
    {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetDeviceLive()
    {
        String name = getRokuDeviceName("livingroom");
        Roku roku = Roku.findByUserDeviceName(name);
        Device device = roku.getDevice();
        System.out.println("DEVICE: " + device);
        Player player = roku.getPlayer();
        System.out.println("PLAYER: " + player);

    }

    @Test
    public void testGetDevice() throws IOException
    {
        final String response = getResourceString(ParserTest.class, "device.xml");

        Request request = Mockito.spy(QueryRequest.forDeviceInfoQuery(createTestDeviceInfo()));

        Mockito.doReturn(response).when(request).send();
        Mockito.doReturn(request).when(mockRoku).createDeviceInfoQueryRequest();
        Mockito.when(mockRoku.getDevice()).thenCallRealMethod();

        Device device = mockRoku.getDevice();
        TestHelper.testDevice(device);

    }

    @Test
    public void testGetPlayer() throws IOException
    {
        final String response = getResourceString(ParserTest.class, "media-player.xml");

        Request request = Mockito.spy(QueryRequest.forMediaPlayerQuery(createTestDeviceInfo()));

        Mockito.doReturn(response).when(request).send();
        Mockito.doReturn(request).when(mockRoku).createMediaPlayerRequest();
        Mockito.when(mockRoku.getPlayer()).thenCallRealMethod();

        Player player = mockRoku.getPlayer();
        TestHelper.testPlayer(player);

    }

    @Test
    public void testFindByUserDeviceName()
    {
        String name = getRokuDeviceName("livingroom");
        Roku roku = Roku.findByUserDeviceName(name);
        Device device = roku.getDevice();

        System.out.println(device);

        Player player = roku.getPlayer();
        System.out.println(player);
    }

    @Test
    public void testPressKey()
    {
        String name = getRokuDeviceName("livingroom");
        Roku roku = Roku.findByUserDeviceName(name);

        roku.pressKey(RokuKey.REWIND);
    }

    @Test
    public void testGetAppsLive()
    {
        String name = getRokuDeviceName("livingroom");
        Roku roku = Roku.findByUserDeviceName(name);

        List<RokuApp> apps = roku.getApps();
        Assertions.assertNotNull(apps, "Roku Apps list is null");
        Assertions.assertEquals(false, apps.isEmpty(), "Roku app list is empty");
    }

    @Test
    public void testGetActiveAppLive()
    {
        String name = getRokuDeviceName("livingroom");
        Roku roku = Roku.findByUserDeviceName(name);

        RokuApp app = roku.getActiveApp();
        Assertions.assertNotNull(app, "Roku Active Apps is null");
    }

    @Test
    public void testSendText()
    {

        String name = getRokuDeviceName("livingroom");
        Roku roku = Roku.findByUserDeviceName(name);

        roku.sendText("resident evil");
    }

    @Test
    public void testGetActiveApp() throws IOException
    {
        final String response = getActiveAppXml();
        final DeviceInfo deviceInfo = createTestDeviceInfo();

        Request request = Mockito.spy(QueryRequest.forActiveApp(deviceInfo));

        Mockito.doReturn(response).when(request).send();
        Mockito.doReturn(request).when(mockRoku).createActiveAppQueryRequest();
        Mockito.when(mockRoku.getActiveApp()).thenCallRealMethod();
        RokuApp app = mockRoku.getActiveApp();

        Assertions.assertNotNull(app, "Active Roku App is null");
        Assertions.assertEquals("YouTube", app.getName(), "Invalid Roku App name");

    }

    @Test
    public void testGetApps() throws IOException
    {
        final String response = getAppsXml();
        final DeviceInfo deviceInfo = createTestDeviceInfo();

        Request request = Mockito.spy(QueryRequest.forApps(deviceInfo));

        Mockito.doReturn(response).when(request).send();
        Mockito.doReturn(request).when(mockRoku).createQueryAppsRequest();
        Mockito.when(mockRoku.getApps()).thenCallRealMethod();
        List<RokuApp> apps = mockRoku.getApps();

        Assertions.assertNotNull(apps, "App list is null");
        Assertions.assertEquals(2, apps.size(), "Invalid Roku App list size");

        List<String> names = apps.stream().map(RokuApp::getName).collect(Collectors.toList());
        String[] expectedNames =
        {
            "Plex - Free Movies & TV", "Tablo TV"
        };

        for (String expected : expectedNames)
        {
            String error = String.format("Missing expected app '%s'", expected);
            Assertions.assertEquals(true, names.contains(expected), error);
        }

    }

    private String getAppsXml()
    {
        return "<apps>\n"
                + "	<app id=\"13535\" type=\"appl\" version=\"6.7.24\">Plex - Free Movies &amp; TV</app>\n"
                + "	<app id=\"44281\" type=\"appl\" version=\"2.17.0\">Tablo TV</app>\n"
                + "</apps>";
    }

    private String getActiveAppXml()
    {
        return "<active-app>\n"
                + "	<app id=\"837\" type=\"appl\" version=\"1.0.93000138\">YouTube</app>\n"
                + "</active-app>";
    }

    protected DeviceInfo createTestDeviceInfo()
    {
        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.setCacheMaxAge(Duration.ofMillis(3600));
        deviceInfo.setDeviceGroup("roku.test");
        deviceInfo.setLocation("http://127.0.0.1/8060");
        deviceInfo.setMacAddress("aa:bb:cc");
        deviceInfo.setSerialNumber("AA000000");
        deviceInfo.setServer("roku-server");

        return deviceInfo;
    }

}
