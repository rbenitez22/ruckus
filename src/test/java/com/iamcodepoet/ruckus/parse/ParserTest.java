/*
 * Copyright 2021 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.ruckus.parse;

import com.iamcodepoet.ruckus.BaseTest;
import com.iamcodepoet.ruckus.TestHelper;
import com.iamcodepoet.ruckus.model.ActiveApp;
import com.iamcodepoet.ruckus.model.Device;
import com.iamcodepoet.ruckus.model.Player;
import com.iamcodepoet.ruckus.model.Plugin;
import com.iamcodepoet.ruckus.model.RokuApp;
import com.iamcodepoet.ruckus.model.RokuApps;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Roberto C. Benitez
 */
public class ParserTest extends BaseTest
{

    public ParserTest()
    {
    }

    @Test
    public void testParsePlayer() throws IOException
    {
        try ( InputStream is = getResource("media-player.xml"))
        {
            Parser<Player> parser = new Parser<>();
            Player player = parser.parse(Player.class, is);

            TestHelper.testPlayer(player);

        }
    }

    @Test
    public void testParseDevice() throws IOException
    {
        try ( InputStream is = getResource("device.xml"))
        {
            Parser<Device> parser = new Parser<>();
            Device device = parser.parse(Device.class, is);

            Assertions.assertEquals("d9d885fc-411c-4e81-b9f6-29eb13ad0aa3", device.getUdn(), "invalid UND");
            Assertions.assertEquals("ZB000000000000", device.getSerialNumber(), "invalid device ID");
            Assertions.assertEquals(true, device.isSecureDevice(), "Invalid value for secure-device");

        }
    }

    @Test
    public void testParsePlayerPluginOnly() throws IOException
    {
        try ( InputStream is = getResource("plugin-only-media-player.xml"))
        {
            Parser<Player> parser = new Parser<>();
            Player player = parser.parse(Player.class, is);

            Plugin plugin = player.getPlugin();
            Assertions.assertNotNull(plugin, "Plugin element is null");

            Assertions.assertEquals("5000000 bps", plugin.getBandwidth(), "Invalid bandwidth value");
            Assertions.assertEquals("YouTube", plugin.getName());
        }
    }

    @Test
    public void testParseRokuApp() throws IOException
    {
        Parser<RokuApp> parser = new Parser<>();
        RokuApp app = parser.parse(RokuApp.class, "<app id=\"19977\" type=\"appl\" version=\"2.7.16\">Spotify Music</app>");
        System.out.println(app);
    }

    @Test
    public void testParseApps() throws IOException
    {
        try ( InputStream is = getResource("roku-app-list.xml"))
        {

            Parser<RokuApps> parser = new Parser<>();
            RokuApps apps = parser.parse(RokuApps.class, is);

            Assertions.assertNotNull(apps, "RokuApps element is null");

            List<RokuApp> appList = apps.getList();
            Assertions.assertNotNull(appList, "RokuApp List is null");
            Assertions.assertFalse(appList.isEmpty(), "RokuApp List is empty");

            Assertions.assertEquals(15, appList.size(), "Invalid RokuApp list size");

        }

    }

    @Test
    public void testParseActiveApp() throws IOException
    {
        try ( InputStream is = getResource("active-app.xml"))
        {
            Parser<ActiveApp> parser = new Parser<>();
            ActiveApp active = parser.parse(ActiveApp.class, is);

            Assertions.assertNotNull(active, "Active Roku App element is null");

            RokuApp app = active.getApp();
            Assertions.assertNotNull(app, "Active App is null");
            Assertions.assertEquals("YouTube", app.getName(), "Invalid App Name");

        }
    }
}
