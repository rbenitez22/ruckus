/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.iamcodepoet.ruckus;

import java.io.IOException;
import java.time.Duration;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Roberto C. Benitez
 */
public class DeviceFinderTest
{

    public DeviceFinderTest()
    {
    }

    /**
     * Local test.
     *
     * @throws IOException thrown if errors occur getting devices
     */
    public void testFindRokuDevices() throws IOException
    {
        DeviceFinder finder = new DeviceFinder();
        List<DeviceInfo> devices = finder.findRokuDevices();
        devices.forEach(System.out::println);
    }

    @Test
    public void testParseDeviceInfo()
    {
        DeviceFinder finder = new DeviceFinder();
        DeviceInfo info = finder.parseDeviceInfo(getDeviceInfoString());

        Assertions.assertEquals(Duration.ofSeconds(3600), info.getCacheMaxAge(), "Invalid Cache Max Age");
        Assertions.assertEquals("AB00000001", info.getSerialNumber());
        Assertions.assertEquals("Roku/10.0.0 UPnP/1.0 Roku/10.0.0", info.getServer());
        Assertions.assertEquals("http://192.111.222.333:8060", info.getLocation());
        Assertions.assertEquals("A111A3B2222DD3Z5E45D5", info.getDeviceGroup());
        Assertions.assertEquals("00:zy:q8:r3:95:rr", info.getMacAddress());

    }

    private String getDeviceInfoString()
    {
        return "[HTTP/1.1 200 OK\n"
                + "Cache-Control: max-age=3600\n"
                + "ST: roku:ecp\n"
                + "USN: uuid:roku:ecp:AB00000001\n"
                + "Ext: \n"
                + "Server: Roku/10.0.0 UPnP/1.0 Roku/10.0.0\n"
                + "LOCATION: http://192.111.222.333:8060/\n"
                + "device-group.roku.com: A111A3B2222DD3Z5E45D5\n"
                + "WAKEUP: MAC=00:zy:q8:r3:95:rr;Timeout=10";
    }

}
