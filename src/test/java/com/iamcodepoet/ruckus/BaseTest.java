/*
 * Copyright 2021 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.ruckus;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 *
 * @author Roberto C. Benitez
 */
public class BaseTest
{

    /**
     * Get the name of a Roku device by the given location name.
     *
     * @param locationName location name
     * @return Roku device name
     */
    protected String getRokuDeviceName(String locationName)
    {
        Path path = Paths.get(System.getProperty("user.home"), ".roku", "roku-info.properties");
        if (!Files.exists(path))
        {
            throw new RokuException("File does not exist. " + path);
        }

        try ( InputStream is = Files.newInputStream(path))
        {
            Properties props = new Properties();
            props.load(is);

            if (props.containsKey(locationName))
            {
                return props.getProperty(locationName);
            }

            String error = String.format("Roku location name '%s' does not exist", locationName);
            throw new RokuException(error);
        }
        catch (IOException e)
        {
            String error = String.format("Error occured reading file '%s'", path);
            throw new RokuException(error, e);
        }
    }

    protected String getResourceString(String name) throws IOException
    {

        return getResourceString(getClass(), name);
    }

    protected String getResourceString(Class<?> context, String name) throws IOException
    {
        StringBuilder response = new StringBuilder();

        try ( InputStream is = getResource(context, name))
        {
            while (true)
            {
                byte[] buff = new byte[1024];
                int count = is.read(buff);
                if (count < 1)
                {
                    break;
                }
                String string = new String(buff, 0, count);
                response.append(string);

            }
        }

        return response.toString();
    }

    protected InputStream getResource(String name) throws IOException
    {
        return getResource(getClass(), name);
    }

    protected InputStream getResource(Class<?> context, String name) throws IOException
    {
        InputStream is = context.getResourceAsStream(name);
        if (is == null)
        {
            URL path = getClass().getResource("");
            String error = String.format("Resource '%s' does not exists in '%s'", name, path);
            throw new IllegalArgumentException(error);
        }
        return is;
    }

}
