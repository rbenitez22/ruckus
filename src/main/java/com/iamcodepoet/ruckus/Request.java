/*
 * Copyright 2021 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.ruckus;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 * @author Roberto C. Benitez
 */
public abstract class Request
{

    private int connectionTimeout = 10_000;
    private int readTimeout = 10_000;
    private final DeviceInfo deviceInfo;
    private final String baseUrl;
    private String method = "GET";

    public Request(DeviceInfo deviceInfo)
    {
        this.deviceInfo = Objects.requireNonNull(deviceInfo, "deviceInfo must not be null");
        this.baseUrl = Objects.requireNonNull(this.deviceInfo.getLocation(), "Divice Location must not be null");
    }

    protected String getMethod()
    {
        return method;
    }

    protected void setMethod(String method)
    {
        this.method = method;
    }

    public int getConnectionTimeout()
    {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout)
    {
        this.connectionTimeout = connectionTimeout;
    }

    public int getReadTimeout()
    {
        return readTimeout;
    }

    public void setReadTimeout(int readTimeout)
    {
        this.readTimeout = readTimeout;
    }

    protected abstract String getUri();

    public String send() throws RokuException
    {
        return send(Collections.emptyMap());
    }

    public String send(Map<String, String> parameters) throws RokuException
    {
        HttpURLConnection conn = null;
        try
        {
            conn = createRequestConnection(parameters);
            return getResponse(conn);
        }
        catch (IOException e)
        {
            String error = String.format("Error connecting to Roku. %s", e.getMessage());
            throw new RokuException(error, e);
        }
        finally
        {
            if (conn != null)
            {
                conn.disconnect();
            }
        }

    }

    protected final String getResponse(HttpURLConnection conn) throws RokuException, IOException
    {
        final String response;
        try ( InputStream is = conn.getInputStream())
        {
            response = Request.this.getResponse(is);
        }
        catch (IOException e)
        {
            throw new RokuException(e.getMessage(), e);
        }

        return response;
    }

    protected HttpURLConnection createRequestConnection(Map<String, String> parameters)
    {
        try
        {
            URL url = createRequestUrl(parameters);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("User-Agent", "Rackus");
            conn.setConnectTimeout(connectionTimeout);
            conn.setReadTimeout(readTimeout);
            conn.setRequestMethod(method);
            conn.setDoOutput(true);

            conn.connect();

            return conn;
        }
        catch (IOException e)
        {
            throw new RokuException(e.getMessage(), e);
        }
    }

    private String getResponse(InputStream is) throws RokuException
    {
        StringBuilder builder = new StringBuilder();
        try
        {
            while (true)
            {
                byte[] buff = new byte[1024];
                int count = is.read(buff);
                if (count < 1)
                {
                    break;
                }

                String string = new String(buff, 0, count);
                builder.append(string);
            }
        }
        catch (IOException e)
        {
            String error = String.format("Error sending requeset. %s", e.getMessage());
            throw new RokuException(error, e);
        }

        return builder.toString();
    }

    private URL createRequestUrl(Map<String, String> parameters) throws RokuException
    {
        String urlSpec = String.join("/", baseUrl, getUri());
        String queryString = createQueryString(parameters);
        if (!queryString.isEmpty())
        {
            urlSpec = urlSpec + "?" + queryString;
        }

        URL url;
        try
        {
            url = new URL(urlSpec);
        }
        catch (MalformedURLException e)
        {
            String error = String.format("A valid URL could not be created.  The request URI or parameters may be invalid. %s", e.getMessage());
            throw new RokuException(error, e);
        }
        return url;
    }

    protected String createQueryString(Map<String, String> parameters)
    {
        return parameters.entrySet().stream()
                .map(mapEntryToQueryTerm())
                .collect(Collectors.joining("&"));
    }

    private Function<Entry<String, String>, String> mapEntryToQueryTerm()
    {
        return entry ->
        {
            String value = URLEncoder.encode(entry.getValue(), Charset.forName("UTF-8"));
            return String.format("%s=%s", entry.getKey(), value);
        };
    }
}
