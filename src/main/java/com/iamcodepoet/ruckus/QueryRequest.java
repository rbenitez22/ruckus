/*
 * Copyright 2021 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.ruckus;

public class QueryRequest extends Request
{

    private final static String BASE_URI = "query";

    private final String uri;

    private QueryRequest(DeviceInfo deviceInfo, String resource)
    {
        super(deviceInfo);
        this.uri = String.join("/", BASE_URI, resource);
    }

    @Override
    protected String getUri()
    {
        return uri;
    }

    public static Request forDeviceInfoQuery(DeviceInfo device)
    {
        return new QueryRequest(device, "device-info");
    }

    public static Request forMediaPlayerQuery(DeviceInfo device)
    {
        return new QueryRequest(device, "media-player");
    }

    public static Request forApps(DeviceInfo device)
    {
        return new QueryRequest(device, "apps");
    }

    public static Request forActiveApp(DeviceInfo device)
    {
        return new QueryRequest(device, "active-app");
    }

}
