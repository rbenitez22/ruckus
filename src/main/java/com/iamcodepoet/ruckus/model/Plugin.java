/*
 * Copyright 2021 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.ruckus.model;

import java.util.Objects;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 *
 * @author Roberto C. Benitez
 */
@Root
public class Plugin
{

    @Attribute
    private String id;

    @Attribute
    private String name;

    @Attribute
    private String bandwidth;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getBandwidth()
    {
        return bandwidth;
    }

    public void setBandwidth(String bandwidth)
    {
        this.bandwidth = bandwidth;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, name, bandwidth);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final Plugin other = (Plugin) obj;
        return new EqualsBuilder()
                .append(id, other.getId())
                .append(name, other.getName())
                .append(bandwidth, other.getBandwidth())
                .build();
    }

    @Override
    public String toString()
    {
        return name;
    }

}
