/*
 * Copyright 2021 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.ruckus.model;

import java.util.Objects;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 *
 * @author Roberto C. Benitez
 */
@Root
public class Player
{

    @Attribute
    private boolean error;

    @Attribute
    private String state;

    @Element(required = false)
    private Plugin plugin;

    @Element(required = false)
    private Format format;

    @Element(required = false)
    private Buffering buffering;

    @Element(required = false, name = "new_stream")
    private NewStream newStream;

    @Element(required = false)
    private String position;

    @Element(required = false)
    private String duration;

    @Element(required = false, name = "is_live")
    private boolean live;

    @Element(required = false, name = "stream_segment")
    private StreamSegment streamSegment;

    public Plugin getPlugin()
    {
        return plugin;
    }

    public void setPlugin(Plugin plugin)
    {
        this.plugin = plugin;
    }

    public Format getFormat()
    {
        return format;
    }

    public void setFormat(Format format)
    {
        this.format = format;
    }

    public Buffering getBuffering()
    {
        return buffering;
    }

    public void setBuffering(Buffering buffering)
    {
        this.buffering = buffering;
    }

    public NewStream getNewStream()
    {
        return newStream;
    }

    public void setNewStream(NewStream newStream)
    {
        this.newStream = newStream;
    }

    public String getPosition()
    {
        return position;
    }

    public void setPosition(String position)
    {
        this.position = position;
    }

    public String getDuration()
    {
        return duration;
    }

    public void setDuration(String duration)
    {
        this.duration = duration;
    }

    public boolean isLive()
    {
        return live;
    }

    public void setLive(boolean live)
    {
        this.live = live;
    }

    public boolean isError()
    {
        return error;
    }

    public void setError(boolean error)
    {
        this.error = error;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        this.state = state;
    }

    public StreamSegment getStreamSegment()
    {
        return streamSegment;
    }

    public void setStreamSegment(StreamSegment streamSegment)
    {
        this.streamSegment = streamSegment;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(plugin, format, buffering, newStream, position, duration);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final Player other = (Player) obj;
        return new EqualsBuilder()
                .append(plugin, other.getPlugin())
                .append(format, other.getFormat())
                .append(buffering, other.getBuffering())
                .append(newStream, other.getNewStream())
                .append(position, other.getPosition())
                .append(duration, other.getDuration())
                .build();
    }

    @Override
    public String toString()
    {
        if (plugin == null)
        {
            return super.toString();
        }

        return plugin.toString();
    }

}
