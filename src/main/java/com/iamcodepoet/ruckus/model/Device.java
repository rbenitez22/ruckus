/* 
 * Copyright 2021 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.ruckus.model;

import java.util.Objects;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 *
 * @author Roberto C. Benitez
 */
@Root
public class Device
{
    
    @Element(name = "udn")
    private String udn;
    
    @Element(name = "serial-number")
    private String serialNumber;
    
    @Element(name = "device-id")
    private String deviceId;
    
    @Element(name = "advertising-id")
    private String advertisingId;
    
    @Element(name = "vendor-name")
    private String vendorName;
    
    @Element(name = "model-name")
    private String modelName;
    
    @Element(name = "model-number")
    private String modelNumber;
    
    @Element(name = "model-region")
    private String modelRegion;
    
    @Element(name = "is-tv")
    private boolean tv;
    
    @Element(name = "is-stick")
    private boolean stick;
    
    @Element(name = "ui-resolution")
    private String uiResolution;
    
    @Element(name = "supports-ethernet")
    private boolean supportsEthernet;
    
    @Element(name = "wifi-mac")
    private String wifiMac;
    
    @Element(name = "wifi-driver")
    private String wifiDriver;
    
    @Element(name = "has-wifi-extender")
    private boolean hasWifiExtender;
    
    @Element(name = "has-wifi-5G-support")
    private boolean hasWifi5GSupport;
    
    @Element(name = "can-use-wifi-extender")
    private boolean canUseEifiExtender;
    
    @Element(name = "ethernet-mac")
    private String ethernetMac;
    
    @Element(name = "network-type")
    private String networkType;
    
    @Element(name = "network-name")
    private String networkName;
    
    @Element(name = "friendly-device-name")
    private String friendlyDeviceName;
    
    @Element(name = "friendly-model-name")
    private String friendlyModelName;
    
    @Element(name = "default-device-name")
    private String defaultDeviceName;
    
    @Element(name = "user-device-name")
    private String userDeviceName;
    
    @Element(name = "user-device-location")
    private String userDeviceLocation;
    
    @Element(name = "build-number")
    private String buildNumber;
    
    @Element(name = "software-version")
    private String softwareVersion;
    
    @Element(name = "software-build")
    private String softwareBuild;
    
    @Element(name = "secure-device")
    private boolean secureDevice;
    
    @Element(name = "language")
    private String language;
    
    @Element(name = "country")
    private String country;
    
    @Element(name = "locale")
    private String locale;
    
    @Element(name = "time-zone-auto")
    private boolean timeZoneAuto;
    
    @Element(name = "time-zone")
    private String timeZone;
    
    @Element(name = "time-zone-name")
    private String timeZoneName;
    
    @Element(name = "time-zone-tz")
    private String timeZoneTz;
    
    @Element(name = "time-zone-offset")
    private int timeZoneOffset;
    
    @Element(name = "clock-format")
    private String clockFormat;
    
    @Element(name = "supports-suspend")
    private String supportsSuspend;
    
    @Element(name = "uptime")
    private long upTime;
    
    @Element(name = "power-mode")
    private String powerMode;
    
    @Element(name = "supports-find-remote")
    private boolean supportsFindRemote;
    
    @Element(name = "find-remote-is-possible")
    private boolean findRemoteIsPossible;
    
    @Element(name = "supports-audio-guide")
    private boolean supportsAudioGuide;
    
    @Element(name = "supports-rva")
    private boolean supportsRva;
    
    @Element(name = "developer-enabled")
    private boolean developerEnabled;
    
    @Element(name = "keyed-developer-id", required = false)
    private String keyedDeveloperId;
    
    @Element(name = "search-enabled")
    private boolean searchEnabled;
    
    @Element(name = "search-channels-enabled")
    private boolean searchchannelsEnabled;
    
    @Element(name = "voice-search-enabled")
    private boolean voiceSearchEnabled;
    
    @Element(name = "notifications-enabled")
    private boolean notificationsEnabled;
    
    @Element(name = "notifications-first-use")
    private boolean notificationsFirstUse;
    
    @Element(name = "supports-private-listening")
    private boolean supportsPrivateListening;
    
    @Element(name = "headphones-connected")
    private boolean headphonesConnected;
    
    @Element(name = "supports-ecs-textedit")
    private boolean supportsEcsTextedit;
    
    @Element(name = "supports-ecs-microphone")
    private boolean supportsEcsMicrophone;
    
    @Element(name = "supports-wake-on-wlan")
    private boolean supportsWakeOnWlan;
    
    @Element(name = "supports-airplay")
    private boolean supportsAirplay;
    
    @Element(name = "has-play-on-roku")
    private boolean hasPlayOnRoku;
    
    @Element(name = "has-mobile-screensaver")
    private boolean hasMobileScreensaver;
    
    @Element(name = "support-url")
    private String supportUrl;
    
    @Element(name = "grandcentral-version")
    private String grandcentralVersion;
    
    @Element(name = "trc-version")
    private String trcVersion;
    
    @Element(name = "trc-channel-version")
    private String trcChannelVersion;
    
    @Element(name = "davinci-version")
    private String davinciVersion;
    
    public String getUdn()
    {
        return udn;
    }
    
    public void setUdn(String udn)
    {
        this.udn = udn;
    }
    
    public String getSerialNumber()
    {
        return serialNumber;
    }
    
    public void setSerialNumber(String serialNumber)
    {
        this.serialNumber = serialNumber;
    }
    
    public String getDeviceId()
    {
        return deviceId;
    }
    
    public void setDeviceId(String deviceId)
    {
        this.deviceId = deviceId;
    }
    
    public String getAdvertisingId()
    {
        return advertisingId;
    }
    
    public void setAdvertisingId(String advertisingId)
    {
        this.advertisingId = advertisingId;
    }
    
    public String getVendorName()
    {
        return vendorName;
    }
    
    public void setVendorName(String vendorName)
    {
        this.vendorName = vendorName;
    }
    
    public String getModelName()
    {
        return modelName;
    }
    
    public void setModelName(String modelName)
    {
        this.modelName = modelName;
    }
    
    public String getModelNumber()
    {
        return modelNumber;
    }
    
    public void setModelNumber(String modelNumber)
    {
        this.modelNumber = modelNumber;
    }
    
    public String getModelRegion()
    {
        return modelRegion;
    }
    
    public void setModelRegion(String modelRegion)
    {
        this.modelRegion = modelRegion;
    }
    
    public boolean isTv()
    {
        return tv;
    }
    
    public void setIsTv(boolean isTv)
    {
        this.tv = isTv;
    }
    
    public boolean isStick()
    {
        return stick;
    }
    
    public void setStick(boolean stick)
    {
        this.stick = stick;
    }
    
    public String getUiResolution()
    {
        return uiResolution;
    }
    
    public void setUiResolution(String uiResolution)
    {
        this.uiResolution = uiResolution;
    }
    
    public boolean isSupportsEthernet()
    {
        return supportsEthernet;
    }
    
    public void setSupportsEthernet(boolean supportsEthernet)
    {
        this.supportsEthernet = supportsEthernet;
    }
    
    public String getWifiMac()
    {
        return wifiMac;
    }
    
    public void setWifiMac(String wifiMac)
    {
        this.wifiMac = wifiMac;
    }
    
    public String getWifiDriver()
    {
        return wifiDriver;
    }
    
    public void setWifiDriver(String wifiDriver)
    {
        this.wifiDriver = wifiDriver;
    }
    
    public boolean hasWifiExtender()
    {
        return hasWifiExtender;
    }
    
    public void setHasWifiExtender(boolean hasWifiExtender)
    {
        this.hasWifiExtender = hasWifiExtender;
    }
    
    public boolean hasWifi5GSupport()
    {
        return hasWifi5GSupport;
    }
    
    public void setHasWifi5GSupport(boolean hasWifi5GSupport)
    {
        this.hasWifi5GSupport = hasWifi5GSupport;
    }
    
    public boolean canUseEifiExtender()
    {
        return canUseEifiExtender;
    }
    
    public void setCanUseEifiExtender(boolean canUseEifiExtender)
    {
        this.canUseEifiExtender = canUseEifiExtender;
    }
    
    public String getEthernetMac()
    {
        return ethernetMac;
    }
    
    public void setEthernetMac(String ethernetMac)
    {
        this.ethernetMac = ethernetMac;
    }
    
    public String getNetworkType()
    {
        return networkType;
    }
    
    public void setNetworkType(String networkType)
    {
        this.networkType = networkType;
    }
    
    public String getNetworkName()
    {
        return networkName;
    }
    
    public void setNetworkName(String networkName)
    {
        this.networkName = networkName;
    }
    
    public String getFriendlyDeviceName()
    {
        return friendlyDeviceName;
    }
    
    public void setFriendlyDeviceName(String friendlyDeviceName)
    {
        this.friendlyDeviceName = friendlyDeviceName;
    }
    
    public String getFriendlyModelName()
    {
        return friendlyModelName;
    }
    
    public void setFriendlyModelName(String friendlyModelName)
    {
        this.friendlyModelName = friendlyModelName;
    }
    
    public String getDefaultDeviceName()
    {
        return defaultDeviceName;
    }
    
    public void setDefaultDeviceName(String defaultDeviceName)
    {
        this.defaultDeviceName = defaultDeviceName;
    }
    
    public String getUserDeviceName()
    {
        return userDeviceName;
    }
    
    public void setUserDeviceName(String userDeviceName)
    {
        this.userDeviceName = userDeviceName;
    }
    
    public String getUserDeviceLocation()
    {
        return userDeviceLocation;
    }
    
    public void setUserDeviceLocation(String userDeviceLocation)
    {
        this.userDeviceLocation = userDeviceLocation;
    }
    
    public String getBuildNumber()
    {
        return buildNumber;
    }
    
    public void setBuildNumber(String buildNumber)
    {
        this.buildNumber = buildNumber;
    }
    
    public String getSoftwareVersion()
    {
        return softwareVersion;
    }
    
    public void setSoftwareVersion(String softwareVersion)
    {
        this.softwareVersion = softwareVersion;
    }
    
    public String getSoftwareBuild()
    {
        return softwareBuild;
    }
    
    public void setSoftwareBuild(String softwareBuild)
    {
        this.softwareBuild = softwareBuild;
    }
    
    public boolean isSecureDevice()
    {
        return secureDevice;
    }
    
    public void setSecureDevice(boolean secureDevice)
    {
        this.secureDevice = secureDevice;
    }
    
    public String getLanguage()
    {
        return language;
    }
    
    public void setLanguage(String language)
    {
        this.language = language;
    }
    
    public String getCountry()
    {
        return country;
    }
    
    public void setCountry(String country)
    {
        this.country = country;
    }
    
    public String getLocale()
    {
        return locale;
    }
    
    public void setLocale(String locale)
    {
        this.locale = locale;
    }
    
    public boolean isTimeZoneAuto()
    {
        return timeZoneAuto;
    }
    
    public void setTimeZoneAuto(boolean timeZoneAuto)
    {
        this.timeZoneAuto = timeZoneAuto;
    }
    
    public String getTimeZone()
    {
        return timeZone;
    }
    
    public void setTimeZone(String timeZone)
    {
        this.timeZone = timeZone;
    }
    
    public String getTimeZoneName()
    {
        return timeZoneName;
    }
    
    public void setTimeZoneName(String timeZoneName)
    {
        this.timeZoneName = timeZoneName;
    }
    
    public String getTimeZoneTz()
    {
        return timeZoneTz;
    }
    
    public void setTimeZoneTz(String timeZoneTz)
    {
        this.timeZoneTz = timeZoneTz;
    }
    
    public int getTimeZoneOffset()
    {
        return timeZoneOffset;
    }
    
    public void setTimeZoneOffset(int timeZoneOffset)
    {
        this.timeZoneOffset = timeZoneOffset;
    }
    
    public String getClockFormat()
    {
        return clockFormat;
    }
    
    public void setClockFormat(String clockFormat)
    {
        this.clockFormat = clockFormat;
    }
    
    public String getSupportsSuspend()
    {
        return supportsSuspend;
    }
    
    public void setSupportsSuspend(String supportsSuspend)
    {
        this.supportsSuspend = supportsSuspend;
    }
    
    public long getUpTime()
    {
        return upTime;
    }
    
    public void setUpTime(long upTime)
    {
        this.upTime = upTime;
    }
    
    public String getPowerMode()
    {
        return powerMode;
    }
    
    public void setPowerMode(String powerMode)
    {
        this.powerMode = powerMode;
    }
    
    public boolean supportsFindRemote()
    {
        return supportsFindRemote;
    }
    
    public void setSupportsFindRemote(boolean supportsFindRemote)
    {
        this.supportsFindRemote = supportsFindRemote;
    }
    
    public boolean isFindRemoteIsPossible()
    {
        return findRemoteIsPossible;
    }
    
    public void setFindRemoteIsPossible(boolean findRemoteIsPossible)
    {
        this.findRemoteIsPossible = findRemoteIsPossible;
    }
    
    public boolean supportsAudioGuide()
    {
        return supportsAudioGuide;
    }
    
    public void setSupportsAudioGuide(boolean supportsAudioGuide)
    {
        this.supportsAudioGuide = supportsAudioGuide;
    }
    
    public boolean supportsRva()
    {
        return supportsRva;
    }
    
    public void setSupportsRva(boolean supportsRva)
    {
        this.supportsRva = supportsRva;
    }
    
    public boolean getDeveloperEnabled()
    {
        return developerEnabled;
    }
    
    public void setDeveloperEnabled(boolean developerEnabled)
    {
        this.developerEnabled = developerEnabled;
    }
    
    public String getKeyedDeveloperId()
    {
        return keyedDeveloperId;
    }
    
    public void setKeyedDeveloperId(String keyedDeveloperId)
    {
        this.keyedDeveloperId = keyedDeveloperId;
    }
    
    public boolean getSearchEnabled()
    {
        return searchEnabled;
    }
    
    public void setSearchEnabled(boolean searchEnabled)
    {
        this.searchEnabled = searchEnabled;
    }
    
    public boolean getSearchchannelsEnabled()
    {
        return searchchannelsEnabled;
    }
    
    public void setSearchchannelsEnabled(boolean searchchannelsEnabled)
    {
        this.searchchannelsEnabled = searchchannelsEnabled;
    }
    
    public boolean getVoiceSearchEnabled()
    {
        return voiceSearchEnabled;
    }
    
    public void setVoiceSearchEnabled(boolean voiceSearchEnabled)
    {
        this.voiceSearchEnabled = voiceSearchEnabled;
    }
    
    public boolean getNotificationsEnabled()
    {
        return notificationsEnabled;
    }
    
    public void setNotificationsEnabled(boolean notificationsEnabled)
    {
        this.notificationsEnabled = notificationsEnabled;
    }
    
    public boolean getNotificationsFirstUse()
    {
        return notificationsFirstUse;
    }
    
    public void setNotificationsFirstUse(boolean notificationsFirstUse)
    {
        this.notificationsFirstUse = notificationsFirstUse;
    }
    
    public boolean supportsPrivateListening()
    {
        return supportsPrivateListening;
    }
    
    public void setSupportsPrivateListening(boolean supportsPrivateListening)
    {
        this.supportsPrivateListening = supportsPrivateListening;
    }
    
    public boolean getHeadphonesConnected()
    {
        return headphonesConnected;
    }
    
    public void setHeadphonesConnected(boolean headphonesConnected)
    {
        this.headphonesConnected = headphonesConnected;
    }
    
    public boolean supportsEcsTextedit()
    {
        return supportsEcsTextedit;
    }
    
    public void setSupportsEcsTextedit(boolean supportsEcsTextedit)
    {
        this.supportsEcsTextedit = supportsEcsTextedit;
    }
    
    public boolean supportsEcsMicrophone()
    {
        return supportsEcsMicrophone;
    }
    
    public void setSupportsEcsMicrophone(boolean supportsEcsMicrophone)
    {
        this.supportsEcsMicrophone = supportsEcsMicrophone;
    }
    
    public boolean supportsWakeOnWlan()
    {
        return supportsWakeOnWlan;
    }
    
    public void setSupportsWakeOnWlan(boolean supportsWakeOnWlan)
    {
        this.supportsWakeOnWlan = supportsWakeOnWlan;
    }
    
    public boolean supportsAirplay()
    {
        return supportsAirplay;
    }
    
    public void setSupportsAirplay(boolean supportsAirplay)
    {
        this.supportsAirplay = supportsAirplay;
    }
    
    public boolean getHasPlayOnRoku()
    {
        return hasPlayOnRoku;
    }
    
    public void setHasPlayOnRoku(boolean hasPlayOnRoku)
    {
        this.hasPlayOnRoku = hasPlayOnRoku;
    }
    
    public boolean getHasMobileScreensaver()
    {
        return hasMobileScreensaver;
    }
    
    public void setHasMobileScreensaver(boolean hasMobileScreensaver)
    {
        this.hasMobileScreensaver = hasMobileScreensaver;
    }
    
    public String getSupportUrl()
    {
        return supportUrl;
    }
    
    public void setSupportUrl(String supportUrl)
    {
        this.supportUrl = supportUrl;
    }
    
    public String getGrandcentralVersion()
    {
        return grandcentralVersion;
    }
    
    public void setGrandcentralVersion(String grandcentralVersion)
    {
        this.grandcentralVersion = grandcentralVersion;
    }
    
    public String getTrcVersion()
    {
        return trcVersion;
    }
    
    public void setTrcVersion(String trcVersion)
    {
        this.trcVersion = trcVersion;
    }
    
    public String getTrcChannelVersion()
    {
        return trcChannelVersion;
    }
    
    public void setTrcChannelVersion(String trcChannelVersion)
    {
        this.trcChannelVersion = trcChannelVersion;
    }
    
    public String getDavinciVersion()
    {
        return davinciVersion;
    }
    
    public void setDavinciVersion(String davinciVersion)
    {
        this.davinciVersion = davinciVersion;
    }
    
    @Override
    public int hashCode()
    {
        return Objects.hash(udn, serialNumber, deviceId, advertisingId, vendorName, modelName, modelNumber, modelRegion,
                tv, stick, uiResolution, supportsEthernet, wifiMac, wifiDriver, hasWifiExtender, hasWifi5GSupport,
                canUseEifiExtender, ethernetMac, networkType, networkName, friendlyDeviceName,
                friendlyModelName, defaultDeviceName, userDeviceName, userDeviceLocation, buildNumber, softwareVersion,
                softwareBuild, secureDevice, language, country, locale, timeZoneAuto, timeZone, timeZoneName, timeZoneTz,
                timeZoneOffset, clockFormat, supportsSuspend, upTime, powerMode, supportsFindRemote, findRemoteIsPossible,
                supportsAudioGuide, supportsRva, developerEnabled, keyedDeveloperId, searchEnabled, searchchannelsEnabled,
                voiceSearchEnabled, notificationsEnabled, notificationsFirstUse, supportsPrivateListening, headphonesConnected,
                supportsEcsTextedit, supportsEcsMicrophone, supportsWakeOnWlan, supportsAirplay, hasPlayOnRoku, hasMobileScreensaver,
                supportUrl, grandcentralVersion, trcVersion, trcChannelVersion, davinciVersion);
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        
        final Device other = (Device) obj;
        return new EqualsBuilder()
                .append(udn, other.udn)
                .append(serialNumber, other.serialNumber)
                .append(deviceId, other.deviceId)
                .append(advertisingId, other.advertisingId)
                .append(vendorName, other.vendorName)
                .append(modelName, other.modelName)
                .append(modelNumber, other.modelNumber)
                .append(modelRegion, other.modelRegion)
                .append(tv, other.tv)
                .append(stick, other.stick)
                .append(uiResolution, other.uiResolution)
                .append(supportsEthernet, other.supportsEthernet)
                .append(wifiMac, other.wifiMac)
                .append(wifiDriver, other.wifiDriver)
                .append(hasWifiExtender, other.hasWifiExtender)
                .append(hasWifi5GSupport, other.hasWifi5GSupport)
                .append(canUseEifiExtender, other.canUseEifiExtender)
                .append(ethernetMac, other.ethernetMac)
                .append(networkType, other.networkType)
                .append(networkName, other.networkName)
                .append(friendlyDeviceName, other.friendlyDeviceName)
                .append(friendlyModelName, other.friendlyModelName)
                .append(defaultDeviceName, other.defaultDeviceName)
                .append(userDeviceName, other.userDeviceName)
                .append(userDeviceLocation, other.userDeviceLocation)
                .append(buildNumber, other.buildNumber)
                .append(softwareVersion, other.softwareVersion)
                .append(softwareBuild, other.softwareBuild)
                .append(secureDevice, other.secureDevice)
                .append(language, other.language)
                .append(country, other.country)
                .append(locale, other.locale)
                .append(timeZoneAuto, other.timeZoneAuto)
                .append(timeZone, other.timeZone)
                .append(timeZoneName, other.timeZoneName)
                .append(timeZoneTz, other.timeZoneTz)
                .append(timeZoneOffset, other.timeZoneOffset)
                .append(clockFormat, other.clockFormat)
                .append(supportsSuspend, other.supportsSuspend)
                .append(upTime, other.upTime)
                .append(powerMode, other.powerMode)
                .append(supportsFindRemote, other.supportsFindRemote)
                .append(findRemoteIsPossible, other.findRemoteIsPossible)
                .append(supportsAudioGuide, other.supportsAudioGuide)
                .append(supportsRva, other.supportsRva)
                .append(developerEnabled, other.developerEnabled)
                .append(keyedDeveloperId, other.keyedDeveloperId)
                .append(searchEnabled, other.searchEnabled)
                .append(searchchannelsEnabled, other.searchchannelsEnabled)
                .append(voiceSearchEnabled, other.voiceSearchEnabled)
                .append(notificationsEnabled, other.notificationsEnabled)
                .append(notificationsFirstUse, other.notificationsFirstUse)
                .append(supportsPrivateListening, other.supportsPrivateListening)
                .append(headphonesConnected, other.headphonesConnected)
                .append(supportsEcsTextedit, other.supportsEcsTextedit)
                .append(supportsEcsMicrophone, other.supportsEcsMicrophone)
                .append(supportsWakeOnWlan, other.supportsWakeOnWlan)
                .append(supportsAirplay, other.supportsAirplay)
                .append(hasPlayOnRoku, other.hasPlayOnRoku)
                .append(hasMobileScreensaver, other.hasMobileScreensaver)
                .append(supportUrl, other.supportUrl)
                .append(grandcentralVersion, other.grandcentralVersion)
                .append(trcVersion, other.trcVersion)
                .append(trcChannelVersion, other.trcChannelVersion)
                .append(davinciVersion, other.davinciVersion)
                .build();
    }
    
    @Override
    public String toString()
    {
        return String.format("%s (%s)", userDeviceName, modelName);
    }
    
}
