/*
 * Copyright 2021 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.ruckus.model;

import java.util.Objects;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 *
 * @author Roberto C. Benitez
 */
@Root
public class StreamSegment
{

    @Attribute(name = "bitrate")
    private long bitRate;

    @Attribute
    private int height;

    @Attribute
    private int width;

    @Attribute(name = "media_sequence")
    private int mediaSequence;

    @Attribute(name = "segment_type")
    private String segmentType;

    @Attribute
    private long time;

    public long getBitRate()
    {
        return bitRate;
    }

    public void setBitRate(long bitRate)
    {
        this.bitRate = bitRate;
    }

    public int getHeight()
    {
        return height;
    }

    public void setHeight(int height)
    {
        this.height = height;
    }

    public int getWidth()
    {
        return width;
    }

    public void setWidth(int width)
    {
        this.width = width;
    }

    public int getMediaSequence()
    {
        return mediaSequence;
    }

    public void setMediaSequence(int mediaSequence)
    {
        this.mediaSequence = mediaSequence;
    }

    public String getSegmentType()
    {
        return segmentType;
    }

    public void setSegmentType(String segmentType)
    {
        this.segmentType = segmentType;
    }

    public long getTime()
    {
        return time;
    }

    public void setTime(long time)
    {
        this.time = time;
    }

    @Override
    public int hashCode()
    {
        int hash = 5;
        hash = 41 * hash + (int) (this.bitRate ^ (this.bitRate >>> 32));
        hash = 41 * hash + this.height;
        hash = 41 * hash + this.width;
        hash = 41 * hash + this.mediaSequence;
        hash = 41 * hash + Objects.hashCode(this.segmentType);
        hash = 41 * hash + (int) (this.time ^ (this.time >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final StreamSegment other = (StreamSegment) obj;
        if (this.bitRate != other.bitRate)
        {
            return false;
        }
        if (this.height != other.height)
        {
            return false;
        }
        if (this.width != other.width)
        {
            return false;
        }
        if (this.mediaSequence != other.mediaSequence)
        {
            return false;
        }
        if (this.time != other.time)
        {
            return false;
        }
        if (!Objects.equals(this.segmentType, other.segmentType))
        {
            return false;
        }
        return true;
    }

}
