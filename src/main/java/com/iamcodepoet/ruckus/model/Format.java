/*
 * Copyright 2021 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.ruckus.model;

import java.util.Objects;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 *
 * @author Roberto C. Benitez
 */
@Root
public class Format
{

    @Attribute
    private String audio;

    @Attribute
    private String captions;

    @Attribute
    private String container;

    @Attribute
    private String drm;

    @Attribute
    private String video;

    @Attribute(name = "video_res")
    private String videoResolution;

    public String getAudio()
    {
        return audio;
    }

    public void setAudio(String audio)
    {
        this.audio = audio;
    }

    public String getCaptions()
    {
        return captions;
    }

    public void setCaptions(String captions)
    {
        this.captions = captions;
    }

    public String getContainer()
    {
        return container;
    }

    public void setContainer(String container)
    {
        this.container = container;
    }

    public String getDrm()
    {
        return drm;
    }

    public void setDrm(String drm)
    {
        this.drm = drm;
    }

    public String getVideo()
    {
        return video;
    }

    public void setVideo(String video)
    {
        this.video = video;
    }

    public String getVideoResolution()
    {
        return videoResolution;
    }

    public void setVideoResolution(String videoResolution)
    {
        this.videoResolution = videoResolution;
    }

    @Override
    public int hashCode()
    {
        int hash = 7;
        return Objects.hash(audio, captions, container, drm, video, videoResolution);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final Format other = (Format) obj;
        return new EqualsBuilder()
                .append(audio, other.getAudio())
                .append(captions, other.getCaptions())
                .append(container, other.getContainer())
                .append(drm, other.getDrm())
                .append(video, other.getVideo())
                .append(videoResolution, other.getVideoResolution())
                .build();
    }

}
