/*
 * Copyright 2021 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.ruckus.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 *
 * @author Roberto C. Benitez
 */
@Root
public class Buffering
{

    @Attribute
    private long current;

    @Attribute
    private long max;

    @Attribute
    private long target;

    public long getCurrent()
    {
        return current;
    }

    public void setCurrent(long current)
    {
        this.current = current;
    }

    public long getMax()
    {
        return max;
    }

    public void setMax(long max)
    {
        this.max = max;
    }

    public long getTarget()
    {
        return target;
    }

    public void setTarget(long target)
    {
        this.target = target;
    }

    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 79 * hash + (int) (this.current ^ (this.current >>> 32));
        hash = 79 * hash + (int) (this.max ^ (this.max >>> 32));
        hash = 79 * hash + (int) (this.target ^ (this.target >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final Buffering other = (Buffering) obj;
        if (this.current != other.current)
        {
            return false;
        }
        if (this.max != other.max)
        {
            return false;
        }
        if (this.target != other.target)
        {
            return false;
        }
        return true;
    }

}
