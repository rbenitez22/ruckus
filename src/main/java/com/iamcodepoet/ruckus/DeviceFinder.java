/* 
 * Copyright 2021 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.ruckus;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Roberto C. Benitez
 */
public class DeviceFinder
{

    private final String ssdpUrl = "239.255.255.250";
    private final int ssdpPort = 1900;
    private final int requestTimeout = 2000;

    /**
     * Use <b>Simple Service Discovery Protocol (SSDP)</b> to get a list of all
     * available Roku devices.
     *
     * @return Roku device information list
     * @throws RokuException thrown if errors occur getting devices
     */
    public List<DeviceInfo> findRokuDevices() throws RokuException
    {
        try
        {

            byte[] data = getSsdbRequestBody().getBytes();
            DatagramPacket sendPacket = new DatagramPacket(data, data.length, InetAddress.getByName(ssdpUrl), ssdpPort);

            final List<DeviceInfo> devices;
            try ( DatagramSocket clientSocket = new DatagramSocket())
            {
                clientSocket.setSoTimeout(requestTimeout);
                clientSocket.send(sendPacket);
                devices = getResponses(clientSocket);
            }
            return devices;
        }
        catch (IOException e)
        {
            String error = String.format("Error reading SSDP response. %s", e.getMessage());
            throw new RokuException(error, e);
        }

    }

    protected DeviceInfo parseDeviceInfo(String string)
    {
        Map<String, String> values = getValueMap(string);

        String cacheControl = values.getOrDefault("Cache-Control", "");
        int maxAge = parseCacheMaxAge(cacheControl);

        String usn = values.getOrDefault("USN", "");
        int pos = usn.lastIndexOf(":");
        String serialNumber = usn.substring(pos + 1);
        String server = values.getOrDefault("Server", "");
        String location = values.getOrDefault("LOCATION", "");
        if (location.endsWith("/"))
        {
            location = location.substring(0, location.length() - 1);
        }

        String deviceGroup = values.getOrDefault("device-group.roku.com", "");
        String mac = getMac(values.getOrDefault("WAKEUP", ""));

        DeviceInfo info = new DeviceInfo();
        info.setCacheMaxAge(Duration.ofSeconds(maxAge));
        info.setSerialNumber(serialNumber);
        info.setServer(server);
        info.setLocation(location);
        info.setDeviceGroup(deviceGroup);
        info.setMacAddress(mac);

        return info;
    }

    private int parseCacheMaxAge(String cacheControl)
    {
        final int maxAge;
        String token = "max-age=";
        int pos = cacheControl.indexOf(token);
        if (pos >= 0)
        {
            String strValue = cacheControl.substring(pos + token.length());
            maxAge = parseInt(strValue, 0);
        }
        else
        {
            maxAge = 0;
        }
        return maxAge;
    }

    private int parseInt(String string, int defaultValue)
    {
        try
        {
            return Integer.parseInt(string);
        }
        catch (NumberFormatException e)
        {
            return defaultValue;
        }

    }

    private Map<String, String> getValueMap(String string)
    {
        String[] lines = string.split("\n");
        Map<String, String> values = new HashMap<>();
        for (String line : lines)
        {
            String[] tokens = line.split(":", 2);
            String name = tokens[0].trim();
            String value = tokens.length > 1 ? tokens[1].trim() : name;
            values.put(name, value);
        }
        return values;
    }

    private String getMac(String string)
    {
        String macLabel = "MAC=";
        int pos1 = string.indexOf(macLabel);
        if (pos1 < 0)
        {
            return "";
        }

        int pos2 = string.indexOf(";", pos1 + macLabel.length());
        if (pos2 >= 0)
        {
            return string.substring(pos1 + macLabel.length(), pos2);
        }

        return string.substring(pos1);

    }

    protected List<DeviceInfo> getResponses(DatagramSocket socket)
    {
        List<DeviceInfo> devices = new ArrayList<>();
        while (true)
        {
            String response = getResponse(socket);
            if (response == null)
            {
                break;
            }

            DeviceInfo device = parseDeviceInfo(response);
            devices.add(device);
        }

        return devices;
    }

    private String getResponse(DatagramSocket socket)
    {
        try
        {
            byte[] data = new byte[1024];

            DatagramPacket packet = new DatagramPacket(data, data.length);

            socket.receive(packet);

            String response = new String(packet.getData(), 0, packet.getLength());
            return response.trim();
        }
        catch (IOException e)
        {
            return null;
        }

    }

    private static String getSsdbRequestBody()
    {
        return "M-SEARCH * HTTP/1.1\n"
                + "Host: 239.255.255.250:1900\n"
                + "Man: \"ssdp:discover\"\n"
                + "ST: roku:ecp\n\n";
    }
}
