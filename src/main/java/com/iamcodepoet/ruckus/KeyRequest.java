/*
 * Copyright 2021 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.ruckus;

import java.net.URLEncoder;
import java.nio.charset.Charset;

public class KeyRequest extends Request
{

    private final static String KEY_PRESS = "keypress";
    private final static String KEY_UP = "keyup";
    private final static String KEY_DOWN = "keydown";

    private final String uri;

    private KeyRequest(DeviceInfo deviceInfo, String keyAction, String key)
    {
        super(deviceInfo);
        setMethod("POST");
        this.uri = String.join("/", keyAction, key);
    }

    @Override
    protected String getUri()
    {
        return uri;
    }

    public static Request forKeyPress(DeviceInfo deviceInfo, RokuKey key)
    {
        return new KeyRequest(deviceInfo, KEY_PRESS, key.getId());
    }

    public static Request forKeyUp(DeviceInfo deviceInfo, RokuKey key)
    {
        return new KeyRequest(deviceInfo, KEY_UP, key.getId());
    }

    public static Request forKeyDown(DeviceInfo deviceInfo, RokuKey key)
    {
        return new KeyRequest(deviceInfo, KEY_DOWN, key.getId());
    }

    public static Request forCharacter(DeviceInfo deviceInfo, char chr)
    {
        String encoded = URLEncoder.encode(String.valueOf(chr), Charset.forName("UTF-8"));
        String resource = String.format("Lit_%s", encoded);
        return new KeyRequest(deviceInfo, KEY_PRESS, resource);
    }
}
