/*
 * Copyright 2021 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.ruckus.parse;

import java.io.IOException;
import java.io.InputStream;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

/**
 * A wrapper for {@link Persister} to parse arbitrary objects.
 *
 * @author Roberto C. Benitez
 * @param <T> type of object to be parsed
 */
public class Parser<T>
{

    public T parse(Class<T> zclass, InputStream is) throws IOException
    {

        Serializer serializer = new Persister();
        try
        {
            return serializer.read(zclass, is);
        }
        catch (Exception e)
        {
            throw new IOException(e.getMessage(), e);
        }
    }

    public T parse(Class<T> zclass, String string) throws IOException
    {
        Serializer serializer = new Persister();
        try
        {
            return serializer.read(zclass, string);
        }
        catch (Exception e)
        {
            throw new IOException(e.getMessage(), e);
        }
    }

}
