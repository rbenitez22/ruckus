/*
 * Copyright 2021 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.ruckus;

import com.iamcodepoet.ruckus.model.ActiveApp;
import com.iamcodepoet.ruckus.model.Device;
import com.iamcodepoet.ruckus.model.Player;
import com.iamcodepoet.ruckus.model.RokuApp;
import com.iamcodepoet.ruckus.model.RokuApps;
import com.iamcodepoet.ruckus.parse.Parser;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Roberto C. Benitez
 */
public class Roku
{

    private final DeviceInfo info;

    public Roku(DeviceInfo info)
    {
        this.info = info;
    }

    public Device getDevice()
    {
        Request request = createDeviceInfoQueryRequest();
        String response = request.send();
        try
        {
            Parser<Device> parser = new Parser<>();
            return parser.parse(Device.class, response);
        }
        catch (IOException e)
        {
            String error = String.format("Error parsing Device from Roku response. %s", e.getMessage());
            throw new RokuException(error, e);
        }
    }

    protected Request createDeviceInfoQueryRequest()
    {
        return QueryRequest.forDeviceInfoQuery(info);
    }

    public Player getPlayer()
    {
        Request request = createMediaPlayerRequest();
        String response = request.send();
        try
        {
            Parser<Player> parser = new Parser<>();
            return parser.parse(Player.class, response);
        }
        catch (IOException e)
        {
            String error = String.format("Error parsing Media Player from Roku response. %s", e.getMessage());
            throw new RokuException(error, e);
        }
    }

    protected Request createMediaPlayerRequest()
    {
        Request request = QueryRequest.forMediaPlayerQuery(info);
        return request;
    }

    /**
     * Press a Roku Key.
     *
     * <p>
     * This is the equivalent of pressing and immediately pressing and releasing
     * a key or calling {@link #pushKey(com.iamcodepoet.ruckus.RokuKey) } and
     * immediately {@link #releaseKey(com.iamcodepoet.ruckus.RokuKey) }.
     * </p>
     *
     * @param key key
     */
    public void pressKey(RokuKey key)
    {
        Request request = KeyRequest.forKeyPress(info, key);
        request.send();
    }

    /**
     * This is the equivalent of pressing a Roku Remote key, and keeping it
     * pressed.
     *
     * @param key key
     */
    public void pushKey(RokuKey key)
    {
        Request request = KeyRequest.forKeyDown(info, key);
        request.send();
    }

    /**
     * This is the equivalent of releasing a currently pressed (and held) Roku
     * Remote key.
     *
     * @param key key
     */
    public void releaseKey(RokuKey key)
    {
        Request request = KeyRequest.forKeyUp(info, key);
        request.send();
    }

    /**
     * Hold (Press) a key for the given amount of time--duration and then
     * release it.
     *
     * @param key key
     * @param duration duration to hold key
     */
    public void holdKey(RokuKey key, Duration duration)
    {
        pushKey(key);

        synchronized (this)
        {
            try
            {
                this.wait(duration.toMillis());
            }
            catch (InterruptedException ignore)
            {
                //ignore
            }
        }

        releaseKey(key);
    }

    /**
     * Send text to the Roku device.
     *
     * <p>
     * It is necessary for the active app to be in a screen that accepts text
     * input.
     * </p>
     *
     * @param text text to send
     */
    public void sendText(String text)
    {
        Objects.requireNonNull(text, "Text must not be null");
        for (char chr : text.toCharArray())
        {
            Request request = KeyRequest.forCharacter(info, chr);
            request.send();
        }
    }

    public List<RokuApp> getApps()
    {
        Request request = createQueryAppsRequest();
        String response = request.send();

        try
        {
            Parser<RokuApps> parser = new Parser<>();
            RokuApps apps = parser.parse(RokuApps.class, response);
            return apps.getList();
        }
        catch (IOException e)
        {
            String error = String.format("Error parsing Roku Apps Response. %s", e.getMessage());
            throw new RokuException(error, e);
        }

    }

    protected Request createQueryAppsRequest()
    {
        return QueryRequest.forApps(info);
    }

    public RokuApp getActiveApp()
    {
        Request request = createActiveAppQueryRequest();
        String response = request.send();

        try
        {
            Parser<ActiveApp> parser = new Parser<>();
            ActiveApp active = parser.parse(ActiveApp.class, response);
            return active.getApp();
        }
        catch (IOException e)
        {
            String error = String.format("Error parsing Roku Active App Response. %s", e.getMessage());
            throw new RokuException(error, e);
        }

    }

    protected Request createActiveAppQueryRequest()
    {
        return QueryRequest.forActiveApp(info);
    }

    public static Roku findByUserDeviceName(String name)
    {
        Objects.requireNonNull(name, "Device Name must not be null");
        DeviceFinder finder = new DeviceFinder();
        List<DeviceInfo> infoList = finder.findRokuDevices();

        for (DeviceInfo info : infoList)
        {
            Roku roku = new Roku(info);
            Device device = roku.getDevice();
            if (name.equals(device.getUserDeviceName()))
            {
                return roku;
            }
        }

        String msg = String.format("No Roku device found for '%s'", name);
        throw new RokuException(msg);
    }
}
