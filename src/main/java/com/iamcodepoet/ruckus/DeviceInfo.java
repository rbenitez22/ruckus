/* 
 * Copyright 2021 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.ruckus;

import java.time.Duration;
import java.util.Objects;
import org.apache.commons.lang3.builder.EqualsBuilder;

/**
 * A data structure to hold Roku device information obtained via <b>Simple
 * Service Discovery Protocol (SSDP)</b>.
 *
 * @author Roberto C. Benitez
 */
public class DeviceInfo
{

    private Duration cacheMaxAge;
    private String serialNumber;
    private String server;
    private String location;
    private String deviceGroup;
    private String macAddress;

    public Duration getCacheMaxAge()
    {
        return cacheMaxAge;
    }

    public void setCacheMaxAge(Duration cacheMaxAge)
    {
        this.cacheMaxAge = cacheMaxAge;
    }

    public String getSerialNumber()
    {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber)
    {
        this.serialNumber = serialNumber;
    }

    public String getServer()
    {
        return server;
    }

    public void setServer(String server)
    {
        this.server = server;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getDeviceGroup()
    {
        return deviceGroup;
    }

    public void setDeviceGroup(String deviceGroup)
    {
        this.deviceGroup = deviceGroup;
    }

    public String getMacAddress()
    {
        return macAddress;
    }

    public void setMacAddress(String macAddress)
    {
        this.macAddress = macAddress;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(cacheMaxAge, serialNumber, server, location, deviceGroup, macAddress);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final DeviceInfo other = (DeviceInfo) obj;
        return new EqualsBuilder()
                .append(cacheMaxAge, other.getCacheMaxAge())
                .append(serialNumber, other.getSerialNumber())
                .append(server, other.getServer())
                .append(location, other.getLocation())
                .append(deviceGroup, other.getDeviceGroup())
                .append(macAddress, other.getMacAddress())
                .build();
    }

    @Override
    public String toString()
    {
        return location;
    }

}
