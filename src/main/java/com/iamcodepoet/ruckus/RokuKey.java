/*
 * Copyright 2021 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.ruckus;

/**
 *
 * @author Roberto C. Benitez
 */
public enum RokuKey
{
    HOME("Home"),
    REWIND("Rev"),
    FAST_FORWARD("Fwd"),
    PLAY("Play"),
    SELECT("Select"),
    LEFT("Left"),
    RIGHT("Right"),
    DOWN("Down"),
    UP("Up"),
    BACK("Back"),
    INSTANT_REPLAY("InstantReplay"),
    INFO("Info"),
    BACKSPACE("Backspace"),
    SEARCH("Search"),
    ENTER("Enter"),
    VOLUME_DOWN("VolumeDown"),
    VOLUME_MUTE("VolumeMute"),
    VOLUME_UP("VolumeUp"),
    POWER_OFF("PowerOff"),
    CHANNEL_UP("ChannelUp"),
    CHANNEL_DOWN("ChannelDown"),
    INPUT_TUNER("InputTuner"),
    INPUT_HDMI1("InputHDMI1"),
    INPUT_HDMI2("InputHDMI2"),
    INPUT_HDMI3("InputHDMI3"),
    INPUT_HDMI4("InputHDMI4"),
    INPUT_AV1("InputAV1");

    private final String id;

    RokuKey(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }

    @Override
    public String toString()
    {
        return id;
    }

}
